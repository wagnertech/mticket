#!/bin/bash
set -e

mkdir -p $1/usr/share/php/mTicket
cp -a php/* $1/usr/share/php/mTicket
cp -a etc $1/usr/share/php/mTicket

# link GenericAdmin
pushd $1/usr/share/php/mTicket >/dev/null
	ln -s ../GenericAdmin .
popd >/dev/null

touch $1/usr/share/php/mTicket/GUI/index.php

# fetch local util
rm $1/usr/share/php/mTicket/util
mkdir $1/usr/share/php/mTicket/util
cp -a php/util/* $1/usr/share/php/mTicket/util/

mkdir -p $1/etc/apache2/sites-available/
cp etc/mTicket.conf $1/etc/apache2/sites-available/

