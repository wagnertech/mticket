<?php
function perform_search() {
    $initiator = $_GET["initiator"];
    $category  = $_GET["category"];
    $status    = $_GET["status"];
    $filter = "";
    $filterstr = "";
    if (strlen($initiator) > 0) $filter .= "Initiator-$initiator ";
    if ($category > 0)  $filter .= "Category-$category ";
    if (strlen($status) > 0)    $filter .= "Status-$status ";
    if (strlen($filter) > 0) $filterstr = "&filter=$filter";
    header("Location: ListView.php?event=ClassSelect&appclass=mTicketUser+1$filterstr");
    exit;
}

// define user
$user = getenv("USER");
if ($user == "") putenv("USER=gui");

// includes
$path = preg_replace("/mTicket.*/", "mTicket", __FILE__);
set_include_path($path . PATH_SEPARATOR . get_include_path());
require_once 'util/Logger.php';
require_once 'util/error_codes.php';
require_once 'GenericAdmin/gui/control/GenadControl.php';

// Logging
$logger = Logger::getInstance();

try {
    // load configuration + class loader
    include 'GUI/UserConfiguration.php';
    Config::setConfiguration($configurationData);
    
    // Log request
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $logger->log(__FILE__, $requestMethod);
    if ($requestMethod == "GET") {
        $logger->log(__FILE__, "GET-Variabeles:");
        foreach ( $_GET as $key => $value) {
            $logger->log(__FILE__, $key . " : " . $value);;
        }
    }
    else {
        $logger->log(__FILE__, "POST-Variabeles:");
        foreach ( $_POST as $key => $value) {
            $logger->log(__FILE__, $key . " : " . $value);;
        }
    }
    $redirect_url = $_SERVER["REDIRECT_URL"];
    $page = preg_replace("/.php$/", "", $redirect_url);
    $page = preg_replace("!.*/!", "", $page);
    $logger->log(__FILE__,"Redirect-URL:".$redirect_url);
    
    // call implementation
    if ($page == "search") perform_search();
    elseif ($page == "create" || $page == "ListView" || $page == "DetailView") {
        $control = new GenadControl();
        $control->doIt();
    }
    else include "GUI/view/UserView.php";
}
catch (Exception $e) {
    $logger->logException(__FILE__, $e);
    echo "<html><body>Error occurred!</body></html>";
}
