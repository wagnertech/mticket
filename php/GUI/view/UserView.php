<?php 
$status = new Status(null);
$status_vals = $status->GetDefdVals();
$category = new CategoryUserEnum(null);
$category_vals = $category->GetDefdVals();
?>
<html>
<head>
<title>mTicket - User - WebGUI</title>
<link rel="stylesheet" type="text/css" href="view/admin.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
</head>
<body>
<!-- TODO: Lokalisierungsmöglichkeit einbauen <? php // echo $ guiText->getText("STRING_"); ?> -->
<p><a href="index.php">Home</a></p><h1>mTicket System</h1>
<hr>
<p><form action="ListView.php">Create ticket: 
	<input type="submit" value="create"/>
	<input type="hidden" name="event" value="Add"/>
	<input type="hidden" name="appclass" value="mTicketUser 1"/>
	<input type="hidden" name="Id" value="0"/></form></p>
<hr>
<p>Search ticket:</p>
<p>Fill in filter parameters to reduce search result.</p>
<table>
<tr></tr>
<tr>
	<td>Initiator</td>
	<td>Category</td>
	<td>Status</td>
	<td>Search:</td></tr>
<tr><form action="search.php">
	<td><input name="initiator"/></td>
	<td><select name="category"><option value="0"/>
		<?php 
		foreach ($category_vals as $key => $value) echo "<option value=\"$key\">$value</option>";
		echo "\n";
		?>
	</select></td>
	<td><select name="status"><option value=""/>
		<?php 
		foreach ($status_vals as $key => $value) echo "<option value=\"$value\">$value</option>";
		echo "\n";
		?>
	</select></td>
	<td><input type="submit" value="search"/></td></form></tr>
</table>
</body>
</html>
