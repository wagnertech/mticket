<?php
// define user
$user = getenv("USER");
if ($user == "") putenv("USER=gui");

// includes
$path = preg_replace("/mTicket.*/", "mTicket", __FILE__);
set_include_path($path . PATH_SEPARATOR . get_include_path());
require_once 'util/Logger.php';
require_once 'util/error_codes.php';
require_once 'GenericAdmin/gui/control/GenadControl.php';

// Logging
$logger = Logger::getInstance();

try {
    // load configuration + class loader
    include 'GUI/AdminConfiguration.php';
    Config::setConfiguration($configurationData);
    
    // Log request
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $logger->log(__FILE__, $requestMethod);
    if ($requestMethod == "GET") {
        $logger->log(__FILE__, "GET-Variabeles:");
        foreach ( $_GET as $key => $value) {
            $logger->log(__FILE__, $key . " : " . $value);;
        }
    }
    else {
        $logger->log(__FILE__, "POST-Variabeles:");
        foreach ( $_POST as $key => $value) {
            $logger->log(__FILE__, $key . " : " . $value);;
        }
    }
    $logger->log(__FILE__,"Redirect-URL:".$_SERVER["REDIRECT_URL"]);
    
    // call implementation
    $control = new GenadControl();
    $control->doIt();
}
catch (Exception $e) {
    $logger->logException(__FILE__, $e);
    echo "<html><body>Error occurred!</body></html>";
}
