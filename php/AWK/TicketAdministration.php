<?php
class TicketAdministration extends GenericAdmin {
    function __construct() {
        // load common Configuration
        include 'AWK/impl/ConfigurationData.php';
        Config::setConfiguration($configurationData);
        
        // Generic Administration configuration data
        include 'AWK/impl/AdminGenadData.php';
        parent::setDscrData($mocd_tab, $atdc_tab);
    }
}

    