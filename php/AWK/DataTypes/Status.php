<?php
// imports
require_once 'GenericAdmin/DataTypes/EnumType.php';

class Status extends EnumType {
    
    /** the possible values for attribute administrative state */
    const ST_NEW  = 1;
    const ST_IW   = 2;
    const ST_DUPL = 3;
    const ST_RES  = 4;
    
    // (string representations of) defined values
    public static $defd_vals = array (
        self::ST_NEW  => "new",
        self::ST_IW   => "in_work",
        self::ST_DUPL => "duplicate",
        self::ST_RES  => "resolved",
    );
    
    public function __construct($value) {
        $this->initInstance('Status', self::$defd_vals, $value);
    }
}
