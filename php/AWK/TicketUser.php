<?php
class TicketUser extends GenericAdmin {
    function __construct() {
        // load common Configuration
        include 'AWK/impl/ConfigurationData.php';
        Config::setConfiguration($configurationData);
        
        // Generic Administration configuration data
        include 'AWK/impl/UserGenadData.php';
        parent::setDscrData($mocd_tab, $atdc_tab);
    }
}

    