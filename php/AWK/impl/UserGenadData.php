<?php

// imports
require_once 'GenericAdmin/DataTypes/AttrProperty.php';

	// Generic Administration configuration data
	$mocd_tab = array (
		1 => array (
			'intern' => 'Ticket',
			'extern' => 'Ticket',
		    'props'   => array (ClassProperty::CP_SET, ClassProperty::CP_CREATE)),
	    2 => array (
			'intern' => 'Category',
			'extern' => 'Category',
	        'props'   => array ()),
		);

	$atdc_tab = array (

	    // Ticket
		array (
			'intern'  => 'id',
			'extern'  => 'Number',
			'moc'     => 'Ticket',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INIT)),
		array (
			'intern'  => 'initiator',
			'extern'  => 'Initiator',
		    'moc'     => 'Ticket',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_MEQU)),
		array (
			'intern'  => 'email',
			'extern'  => 'e-mail',
		    'moc'     => 'Ticket',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'cat_id',
			'extern'  => 'Category',
		    'moc'     => 'Ticket',
		    //'rel_moc' => 2,
		    'gui_cl'  => 'CategoryUserEnum',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_MEQU)),
	    array (
	        'intern'  => 'status',
	        'extern'  => 'Status',
	        'typ_cl'  => 'Status',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_INIT, AttrProperty::AP_MEQU)),
	    array (
			'intern'  => 'description',
			'extern'  => 'Description',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET)),
	    array (
	        'intern'  => 'comments',
	        'extern'  => 'Comments',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INIT)),
	    array (
	        'intern'  => 'comment',
	        'extern'  => 'NewComment',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_SET, AttrProperty::AP_INIT)),
	    array (
	        'intern'  => 'last_update',
	        'extern'  => 'LastUpdate',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INIT),
	        'typ_cl'  => 'GADateTime'
	    ),

	    // Category
	    array (
	        'intern'  => 'name',
	        'extern'  => 'Name',
	        'moc'     => 'Category',
	        'props'   => array (AttrProperty::AP_GET)),
	);

?>
