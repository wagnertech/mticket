<?php

// imports
require_once 'GenericAdmin/DataTypes/AttrProperty.php';

	// Generic Administration configuration data
	$mocd_tab = array (
		1 => array (
			'intern' => 'Stamm',
			'extern' => 'Personenstamm'),
		2 => array (
			'intern' => 'Grundstuck',
			'extern' => 'Grundstück'),
		3 => array (
			'intern' => 'Beitrage',
			'extern' => 'Beiträge'),
// 		4 => array (
// 			'intern' => 'Werkzeug',
// 			'extern' => 'Werkzeug')
		);

	$atdc_tab = array (

	    // Personenstamm
		array (
			'intern'  => 'nachname',
			'extern'  => 'Nachname',
			'moc'     => 'Stamm',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'vorname',
			'extern'  => 'Vorname',
			'moc'     => 'Stamm',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'stasse',
			'extern'  => 'Straße',
			'moc'     => 'Stamm',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'plz',
			'extern'  => 'PLZ',
			'moc'     => 'Stamm',
			'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'ort',
			'extern'  => 'Ort',
			'moc'     => 'Stamm',
			'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'zahlweise',
	        'extern'  => 'Zahlweise',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET),
	        'typ_cl'  => 'Zahlweise'),
	    array (
	        'intern'  => 'iban',
	        'extern'  => 'IBAN',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'bic',
	        'extern'  => 'BIC',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'kommentar',
	        'extern'  => 'Kommentar',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'Grundstuck',
	        'extern'  => 'Versicherte_Grundstücke',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_LIST, AttrProperty::AP_REL),
	        'rel_moc' => 2), //Grundstücke
	    array (
	        'intern'  => 'Beitrag',
	        'extern'  => 'Zahlungen',
	        'moc'     => 'Stamm',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_LIST, AttrProperty::AP_REL),
	        'rel_moc' => 3), //Zahlungen
	    
	    // Grundstück
		array (
			'intern'  => 'wie_anschrift',
			'extern'  => 'wie_Anschrift',
			'moc'     => 'Grundstuck',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_BOOL)),
	    array (
	        'intern'  => 'strasse',
	        'extern'  => 'Straße',
	        'moc'     => 'Grundstuck',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'plz',
	        'extern'  => 'PLZ',
	        'moc'     => 'Grundstuck',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'ort',
	        'extern'  => 'Ort',
	        'moc'     => 'Grundstuck',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'kommentar',
	        'extern'  => 'Kommentar',
	        'moc'     => 'Grundstuck',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    
	    // Beiträge
		array (
			'intern'  => 'jahr',
			'extern'  => 'Jahr',
			'moc'     => 'Beitrage',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INT)),
	    array (
	        'intern'  => 'zahlweise',
	        'extern'  => 'Zahlweise',
	        'moc'     => 'Beitrage',
	        'props'   => array (AttrProperty::AP_GET),
	        'typ_cl'  => 'Zahlweise'),
	    array (
	        'intern'  => 'kommentar',
	        'extern'  => 'Kommentar',
	        'moc'     => 'Beitrage',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		);

?>
