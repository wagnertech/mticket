
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- ticket
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ticket`;

CREATE TABLE `ticket`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `mod` INTEGER NOT NULL,
    `initiator` VARCHAR(100) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `cat_id` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,
    `description` LONGTEXT NOT NULL,
    `last_update` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `ticket_FI_1` (`cat_id`),
    CONSTRAINT `ticket_FK_1`
        FOREIGN KEY (`cat_id`)
        REFERENCES `category` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
    `id` INTEGER NOT NULL,
    `mod` INTEGER NOT NULL,
    `name` CHAR(10) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- comment
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `date` INTEGER NOT NULL,
    `comment` VARCHAR(1000) NOT NULL,
    `ticket_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `comment_FI_1` (`ticket_id`),
    CONSTRAINT `comment_FK_1`
        FOREIGN KEY (`ticket_id`)
        REFERENCES `ticket` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB CHARACTER SET='utf8';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
