<?php



/**
 * This class defines the structure of the 'ticket' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.propel.map
 */
class TicketTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'propel.map.TicketTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ticket');
        $this->setPhpName('Ticket');
        $this->setClassname('Ticket');
        $this->setPackage('propel');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('mod', 'Mod', 'INTEGER', true, null, null);
        $this->addColumn('initiator', 'Initiator', 'VARCHAR', true, 100, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 50, null);
        $this->addForeignKey('cat_id', 'CatId', 'INTEGER', 'category', 'id', true, null, null);
        $this->addColumn('status', 'Status', 'INTEGER', true, null, null);
        $this->addColumn('description', 'Description', 'CLOB', true, null, null);
        $this->addColumn('last_update', 'LastUpdate', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Category', 'Category', RelationMap::MANY_TO_ONE, array('cat_id' => 'id', ), null, null);
        $this->addRelation('Comment', 'Comment', RelationMap::ONE_TO_MANY, array('id' => 'ticket_id', ), 'CASCADE', null, 'Comments');
    } // buildRelations()

} // TicketTableMap
