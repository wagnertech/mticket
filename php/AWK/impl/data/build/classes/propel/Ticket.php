<?php

/**
 * Skeleton subclass for representing a row from the 'ticket' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.propel
 */

require_once 'GenericAdmin/impl/GenadException.php';

class Ticket extends BaseTicket
{
    private $mail_sent = false;
    
    private function send_mail($recipient, $subject, $message) {
        if (! $this->mail_sent) {
            if (file_exists("/usr/bin/mail")) return mail($recipient, $subject, $message);
            $logger = Logger::getInstance();
            $logger->log("Ticket", $recipient);
            $logger->log("Ticket", $subject);
            $logger->log("Ticket", $message);
        }
        return true;
    }
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME) {
        if ($name == 'comment') $this->setComment($value);
        else if ($name == 'comments') ; // do nothing
        else parent::setByName($name, $value, $type);
    }
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME) {
        if ($name == 'comment') return $this->getComment();
        else if ($name == 'comments') {
            $carr = array();
            $comments = $this->getComments();
            foreach ($comments as $c) $carr[] = date("Y-m-d H:i:s", $c->getDate())." / ".$c->getComment();
            return $carr;
            
        }
        else return parent::getByName($name, $type);
    }
    public function getComment() {
		return "";
	}
	public function setComment($comment) {
	    if (strlen($comment) > 0) $this->setComments($this->getComments()." -- ".$comment);
	}
	public function getStatus() {
	    return new Status(parent::getStatus());
	}
	public function setStatus($v) {
	    parent::setStatus($v->getValue());
	}
	public function save(PropelPDO $con = null)
	{
	    $config = Config::getInstance();
	    $this->setLastUpdate(time());
		if ($this->isNew() || $this->mod >= 999999) {
			$this->setMod(0);
		} else {
			$this->setMod($this->mod + 1);
		}
		if ($this->isNew()) {
	       // cannot be checked after save
	       $is_new = true;
		}
	    $affectedRows = parent::save($con);
	    
	    // email notification
	    $desc = $this->getDescription();
	    $id = $this->getId();
	    $name = $this->getInitiator();
	    $category = $this->getCategory()->getName();
	    $status = $this->getStatus();
	    $address = $this->getEmail();
	    if ($is_new) {
		    // send e-mail to new address
		    $newaddress = $config->getConfig("NewAddress");
		    $message = <<<NEWMSG
Hello $name,
a new ticket is in system:
--
Category: $category

$desc
--
See: http://wagnertech.de/mTicket/user/ListView.php?event=Info&appclass=mTicketUser+1&Id=$id
NEWMSG;
            if ($newaddress) {
                $rc = $this->send_mail($newaddress, "New mTicket", $message);
                if (! $rc) throw new Exception ("Cannot send e-mail to $newaddress");
            }
            $rc = $this->send_mail($address, "New mTicket", $message);
            if (! $rc) throw new GenadException ("Cannot send e-mail to $address", GenericAdmin::EC_ATT_ERR);
		}
		else {
		    // ticket changed
		    $comments = $this->getComments();
		    $message = <<<CHGMSG
Hello $name,
your ticket changed:
--
Category: $category

$desc

$comments

Status: $status
--
See: http://wagnertech.de/mTicket/user/ListView.php?event=Info&appclass=mTicketUser+1&Id=$id
CHGMSG;
            $rc = $this->send_mail($address, "mTicket changed", $message);
            if (! $rc) throw new GenadException ("Cannot send e-mail to $address", GenericAdmin::EC_ATT_ERR);
		}
		$this->mail_sent = true;
		return $affectedRows;
	}

}
