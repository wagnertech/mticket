<?php



/**
 * Skeleton subclass for representing a row from the 'category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.propel
 */
class Category extends BaseCategory
{
	public function save(PropelPDO $con = null)
	{
		if ($this->isNew() || $this->mod >= 999999) {
			$this->setMod(0);
		} else {
			$this->setMod($this->mod + 1);
		}
		return parent::save($con);
	}

}
