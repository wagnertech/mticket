<?php
// This file generated by Propel 1.7.2-dev convert-conf target
// from XML runtime conf file /home/debian10/mTicket/php/AWK/impl/data/runtime-conf.xml
$conf = array (
  'datasources' => 
  array (
    'mticket' => 
    array (
      'adapter' => 'mysql',
      'connection' => 
      array (
        'dsn' => 'mysql:host=localhost;dbname=mticket',
        'user' => 'mticket',
        'password' => 'mTicket',
        'settings' => 
        array (
          'charset' => 
          array (
            'value' => 'utf8',
          ),
        ),
      ),
    ),
    'default' => 'mticket',
  ),
  'generator_version' => '1.7.2-dev',
);
$conf['classmap'] = include(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'classmap-propel-conf.php');
return $conf;