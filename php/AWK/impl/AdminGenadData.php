<?php

// imports
require_once 'GenericAdmin/DataTypes/AttrProperty.php';

	// Generic Administration configuration data
	$mocd_tab = array (
		1 => array (
			'intern' => 'Ticket',
			'extern' => 'Ticket'),
		2 => array (
			'intern' => 'Category',
			'extern' => 'Category'),
		);

	$atdc_tab = array (

	    // Ticket
		array (
			'intern'  => 'id',
			'extern'  => 'Number',
			'moc'     => 'Ticket',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INIT)),
		array (
			'intern'  => 'initiator',
			'extern'  => 'Initiator',
		    'moc'     => 'Ticket',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'email',
			'extern'  => 'e-mail',
		    'moc'     => 'Ticket',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
		array (
			'intern'  => 'cat_id',
			'extern'  => 'Category',
		    'moc'     => 'Ticket',
		    //'rel_moc' => 2,
		    'gui_cl'  => 'CategoryEnum',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'status',
	        'extern'  => 'Status',
	        'typ_cl'  => 'Status',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET, AttrProperty::AP_INIT)),
	    array (
			'intern'  => 'description',
			'extern'  => 'Description',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
	        'intern'  => 'comments',
	        'extern'  => 'Comments',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_LIST)),
	    array (
	        'intern'  => 'comment',
	        'extern'  => 'NewComment',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_INIT)),
	    array (
	        'intern'  => 'last_update',
	        'extern'  => 'LastUpdate',
	        'moc'     => 'Ticket',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_INIT),
	        'typ_cl'  => 'GADateTime'
	    ),
	    
	    // Category
	    array (
	        'intern'  => 'id',
	        'extern'  => 'Number',
	        'moc'     => 'Category',
	        'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	    array (
			'intern'  => 'name',
			'extern'  => 'Name',
		    'moc'     => 'Category',
		    'props'   => array (AttrProperty::AP_GET, AttrProperty::AP_SET)),
	);

?>
